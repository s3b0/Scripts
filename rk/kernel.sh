cd kernel
make kernel.img -j$(grep processor /proc/cpuinfo | awk '{field=$NF};END{print field+1}')
cd ..
rm -f RKTools/RK3188DevelopTool/rockdev/Image/kernel.img
mkdir -p RKTools/RK3188DevelopTool/rockdev/Image
cp kernel/kernel.img RKTools/RK3188DevelopTool/rockdev/Image/
#cp kernel/arch/arm/boot/zImage kernel/arch/arm/boot/Image
cp kernel/arch/arm/boot/Image out/target/product/rk30sdk/kernel

