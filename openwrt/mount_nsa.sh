#!/bin/sh
SERVER="nsa.dom"
SLEEP=10
INTERFACE='eth0.1'
MAC='ec:43:f6:bd:e8:e4'
LOG_FILE='/overlay/mpd/scripts/log.log'


pingToServer()
{
	while true
	do
		echo "Ping do $SERVER..."
    		/bin/ping -c 1 $SERVER >> $LOG_FILE 2>&1
    		if [ $? -eq 0 ]
    		then
			break
   		else
			echo "Zasypiam na $SLEEP sekund..." >> $LOG_FILE
   			sleep $SLEEP
   		fi
	done
}

echo "Budze hosta $SERVER..." > $LOG_FILE
/usr/bin/etherwake -D -i $INTERFACE $MAC >> $LOG_FILE 2>&1
pingToServer
echo "Host $SERVER jest online..." >> $LOG_FILE
sleep 60
echo "Montuje zasob MPD..." >> $LOG_FILE
/bin/mount -t cifs //nsa.dom/music /overlay/mpd/music -o port=139,netbiosname=tplink,user=user,pass=pass,dom=dom,iocharset=utf8,file_mode=0777,dir_mode=0777 >> $LOG_FILE 2>&1
if [ $? -eq 0 ]
then
	echo 'Zamontowano zasob MPD poprawnie' >> $LOG_FILE
else
	echo 'Nieudalo sie zamontowac zasobu dla MPD' >> $LOG_FILE
	exit 1
fi
sleep $SLEEP
echo 'Uruchamiam MPD' >> $LOG_FILE
/etc/init.d/mpd start >> $LOG_FILE 2>&1
exit 0
