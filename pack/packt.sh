#!/bin/bash
# only bash supports process substitution!! < < sign!
#set -x
#TODO when epub not found try to download pdf, check epub size (if ther is only pdf epub will be 0 bytes)
RED='\033[0;31m'
YELLOW='\033[1;33m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color
FQDN='https://www.packtpub.com'
LOGIN_PAGE_NAME="login"
LOGIN_PAGE="$FQDN"
USER_NAME_FORM_PARAM_NAME='email'
USER_NAME="$1"
PASSWORD_PARAM_FORM_NAME='password'
PASSWORD="$2"
USER_NAME_AND_PASSWORD_PARAM="$USER_NAME_FORM_PARAM_NAME=$USER_NAME&$PASSWORD_PARAM_FORM_NAME=$PASSWORD"
COOKIE_NAME='cookie.txt'
BOOKS_PAGE='my-ebooks'
BOOKS_PAGE_URL="$FQDN/account/$BOOKS_PAGE"
VIDEOS_PAGE='my-videos'
VIDEOS_PAGE_URL="$FQDN/account/$VIDEOS_PAGE"
BOOKS_LIST_DIV="$BOOKS_PAGE.div.html"
VIDEOS_LIST_DIV="$VIDEOS_PAGE.div.html"
BOOKS_CATALOG='books'
VIDEOS_CATALOG='videos'
FORM_ID=$(curl -sL $FQDN | xmllint --encode UTF-8 --format --html --noent --xpath '//form[@id="packt-user-login-form"]//input[@type="hidden" and @name="form_build_id"]' - 2>/dev/null | sed 's|.*value="\(form-.*\)".*|\1|')
LOGIN_PARAMS="email=$USER_NAME&password=$PASSWORD&op=Login&form_build_id=$FORM_ID&form_id=packt_user_login_form"

if [ -e $LOGIN_PAGE_NAME ]
then
    rm $LOGIN_PAGE_NAME
fi
if [ -e $COOKIE_NAME ]
then
    rm $COOKIE_NAME
fi
# Drukuje na stdout w kolorze czerwień
# $1 - tekst do wydrukowania
function printRed {
    echo -e "${RED}$1${NC}\n"
}
# Drukuje na stdout w kolorze żółć
# $1 - tekst do wydrukowania
function printYellow {
    echo -e "${YELLOW}$1${NC}"
}
# Drukuje na stdout w kolorze biały
# $1 - tekst do wydrukowania
function printWhite {
    echo -e "${WHITE}$1${NC}"
}
# Czyści katalog roboczy
function clean() {
    rm $BOOKS_PAGE* 2>/dev/null
    rm $VIDEOS_PAGE* 2>/dev/null
    rm $COOKIE_NAME 2>/dev/null
    rm logout* 2>/dev/null
}
# Tworzy katalog jeśli nie istnieje
# $1 - pełna ścieżka do katalogu
function createCatalog() {
    if [ ! -d "$1" ]
    then
        mkdir -p "$1"
    fi
}
# Sprawdza kod zwrócony przez wywołane uprzednio polecenie
# $1 - nazwa polecenia
function checkCommand {
    if [ $? != 0 ]
    then
        printRed '!!!!!!!!!!!!!!!!!!'
        printRed "Polecenie w lini $1 zawiodło!"
        printRed '!!!!!!!!!!!!!!!!!!'
        clean
        exit 1
    fi
}
# Pobiera zawartość wskazaną w parametrze $2
# $1 - pełna ścieżka do zapisu elementu z $2
# $2 - ścieżka sieciowa do elementu
# $3 - typ pobierania (Kod,Okładka,Książka,Video)
function download {
    if [ ! -s $1 ]
    then
        printYellow "Pobieram $3 - $1"
        case $3 in
            'Kod') # zipy muszą być poprawne
                wget -O "$1" --load-cookies "$COOKIE_NAME" "$2"
#                checkCommand $LINENO
                unzip -T $1 &>/dev/null #sprawdź zip-a jeśli niepoprawny usuń (nie ma kodów przy danym elemencie a pack zwrócil dla url kodów 200)
                if [ $? != 0 ]
                then
                    printWhite "Zip $1 dla $2 niepoprawny usuwam."
                    rm $1 &>/dev/null # pack zwraca 200 dla nieistniejących kodów przy plikach video, przy książkach jest OK
                fi
            ;;
            'Okładka') # niektóre okładki generują Internal server error :D - także bez sprawdzania kodu powrotu
                wget -O "$1" --load-cookies "$COOKIE_NAME" "$2"
            ;; 
            *)
                wget -O "$1" --load-cookies "$COOKIE_NAME" "$2"
                checkCommand $LINENO
         esac
    else
        printYellow "Nie pobieram $1 ($3) istnieje"
    fi
}
# Poprawnie dekoduje nazwę(tytuł) książki/video
# $1 - pełna linia z pliku html z tytułem (łącznie ze znacznikami) której nie udało się odkodować za pomocą UTF-8
function realName {
    # recode: Ambiguous output in step `HTML_4.0..ISO-10646-UCS-2'
    printRed "Błąd w kodowaniu - próba z ISO-10646-UCS-2"
    local DIR_NAME=$(echo $1 | grep -Po '(?<=title=\").*(?=\")' | tr -d / | recode html..ISO-10646UCS-2 )
    checkCommand $LINENO
    DIR_NAME=${DIR_NAME:2}
    echo $DIR_NAME
}
# Tworzy nazwę katalogu w jakim zapisze się pobrane elementy
# $1 - tytuł książki/video
# $2 - pełna linia z pliku html z tytułem (łącznie ze znacznikami)
function dirName {
    local DIR_NAME=$(echo "$1" | recode html..ascii)
    if [ $? = 1 ]
    then #recode html..ISO-10646-UCS-2 replacement
        DIR_NAME=$(realName "$2")
    fi
    echo ${DIR_NAME//:/_} #usuń : z nazwy
}
# Generuje/pobiera ścieżkę sieciową do okładki
# $1 - tytuł książki/video
# $2 - nazwa elementu div książek/video
function getCoverPath {
    local COVER=$(xmllint --encode UTF-8 --html --xpath 'string(//div[@class="product-line unseen" and @title="'$1'"]//a//img/@src)' $2 2>/dev/null)
    checkCommand $LINENO
    echo "http:$COVER"
}
# Wyłuskuje id książki/video
# $1 - tytuł książki/video
# $2 - nazwa elementu div książek/video
function getNID {
    local NID=$(xmllint --encode UTF-8 --html --xpath 'string(//div[@class="product-line unseen" and @title="'$1'"]/@nid)' $2 2>/dev/null)
    checkCommand $LINENO
    echo $NID
}
# $1 - tytuł książki/video
function getTitle {
    local TITLE=$(echo $1 | grep -Po '(?<=title=\").*(?=\")' | recode html..utf-8)
    echo $TITLE
}
function escape {
  echo "$1" | tr '[:punct:]' '_'
}
### MAIN ###
clean
createCatalog $BOOKS_CATALOG
createCatalog $VIDEOS_CATALOG

#zaloguj
printYellow "Loguje ... $FQDN/?$LOGIN_PARAMS"
wget --save-cookies $COOKIE_NAME --delete-after --post-data $LOGIN_PARAMS $FQDN #najpier siê zaloguj
#checkCommand $LINENO #nie sprawdzaj tutaj kodu błędu!
#potem ściągnij sobie listę swoich ebook-ów
printYellow "Pobieram strone $BOOKS_PAGE ..."
wget --load-cookies $COOKIE_NAME $BOOKS_PAGE_URL -O $BOOKS_PAGE
checkCommand $LINENO
printYellow "Pobieram strone $VIDEOS_PAGE ..."
wget --load-cookies $COOKIE_NAME $VIDEOS_PAGE_URL
checkCommand $LINENO
printYellow "Przetwarzam strone $BOOKS_PAGE"
#xmllint --format --encode UTF-8 --html --noent --xpath '//div[descendant::h1[text()="My eBooks"]]/div[@class="product-line unseen"]/node()' $BOOKS_PAGE 2>/dev/null 1> $BOOKS_LIST_DIV
xmllint --format --encode UTF-8 --html --noent --xpath '//div[descendant::h1[text()="My eBooks "]]//div[@id="product-account-list"]/node()' $BOOKS_PAGE 2>/dev/null 1>$BOOKS_LIST_DIV
checkCommand $LINENO
printYellow "Przetwarzam strone $VIDEOS_PAGE"
#xmllint --format --encode UTF-8 --html --noent --xpath '//div[descendant::h1[text()="My Videos"]]/div[@class="product-line unseen"]/node()' $VIDEOS_PAGE 2>/dev/null 1>$VIDEOS_LIST_DIV
xmllint --format --encode UTF-8 --html --noent --xpath '//div[descendant::h1[text()="My Videos "]]//div[@id="product-account-list"]/node()' $VIDEOS_PAGE 2>/dev/null 1>$VIDEOS_LIST_DIV
checkCommand $LINENO
BOOK_TITLES=$(xmllint --encode UTF-8 --html --xpath '//div[@class="product-line unseen"]/@title' $BOOKS_LIST_DIV 2>/dev/null)
checkCommand $LINENO
VIDEO_TITLES=$(xmllint --encode UTF-8 --html --xpath '//div[@class="product-line unseen"]/@title' $VIDEOS_LIST_DIV 2>/dev/null)
checkCommand $LINENO
SAVEIFS=$IFS
IFS=$'\n'
## VIDEOS ##
while read -r line
do
    VIDEO_TITLE=$(getTitle $line)
    if [ ! -z "$VIDEO_TITLE" ] 
    then
        DIR_NAME=$(dirName $VIDEO_TITLE $line)
        DIR="$VIDEOS_CATALOG/$DIR_NAME"
        createCatalog "$DIR"
        #get book cover
        COVER=$(getCoverPath $VIDEO_TITLE $VIDEOS_LIST_DIV)
        #get book nid
        VNID=$(getNID $VIDEO_TITLE $VIDEOS_LIST_DIV)
        VIDEO="$FQDN/video_download/$VNID"
        CODE="$FQDN/code_download/$VNID"
        printWhite "Pobieram $VIDEO_TITLE cover: $COVER video: $VIDEO cnid: $CODE do $DIR"
        download "$DIR/cover.jpg" "$COVER" 'Okładka'
        download "$DIR/video.zip" "$VIDEO" 'Video'
        download "$DIR/code.zip" "$CODE" 'Kod'
        echo '----------------------------------------------------------------'
    fi
done < <(echo $VIDEO_TITLES | sed "s|title|\ntitle|g")
## BOOKS ##
while read -r line
do
    BOOK_TITLE=$(getTitle $line)
    if [ ! -z "$BOOK_TITLE" ]
    then
        DIR_NAME=$(dirName "$BOOK_TITLE" $line)
        DIR="${BOOKS_CATALOG}/$(escape "$DIR_NAME")"
        createCatalog "$DIR"
        #get book cover
        COVER=$(getCoverPath "$BOOK_TITLE" "$BOOKS_LIST_DIV")
        #get book nid
        BNID=$(getNID "$BOOK_TITLE" "$BOOKS_LIST_DIV")
        #get book code nid
        CNID=$(xmllint --encode UTF-8 --html --xpath 'string(//div[@class="product-line unseen" and @title="'$BOOK_TITLE'"]//a/@nid)' "$BOOKS_LIST_DIV" 2>/dev/null)
        checkCommand $LINENO
        if [ -z "$CNID" ]
        then
            CNID=$(xmllint --encode UTF-8 --html --xpath 'string(//div[@class="product-line unseen" and @title="'$BOOK_TITLE'"]//a[contains(@href, "code_download")]/@href)' "$BOOKS_LIST_DIV" 2>/dev/null)
            checkCommand $LINENO
            CODE="$FQDN/$CNID"
        else
            CODE="$FQDN/code_download/$CNID"
        fi
        BOOK="$FQDN/ebook_download/$BNID"
        printWhite "Pobieram okładkę cover: $COVER"
        download "$DIR/cover.jpg" $COVER 'Okładka'
        #filename with / are not allowed in linux
        #todo escape ' from titles
        #ESCAPED_BOOT_TITLE="${BOOK_TITLE////_}" 
        ESCAPED_BOOT_TITLE="$(escape "$BOOK_TITLE")"
        #TODO add switch between file types
        # PDF
        #printWhite "Pobieram PDF $BOOK_TITLE book: $BOOK cnid: $CODE do $DIR"
        #download "${DIR}/${ESCAPED_BOOT_TITLE}.pdf" "${BOOK}/pdf" 'Książka'
        # EPUB
        printWhite "Pobieram EPUB $BOOK_TITLE book: $BOOK cnid: $CODE do $DIR"
        download "${DIR}/${ESCAPED_BOOT_TITLE}.epub" "${BOOK}/epub" 'Książka'
        # MOBI
        #printWhite "Pobieram MOBI $BOOK_TITLE book: $BOOK cnid: $CODE do $DIR"
        #download "${DIR}/${ESCAPED_BOOT_TITLE}.mobi" "$BOOK/mobi" 'Książka'
        if [ ! -z "$CODE" ]
        then
            download "$DIR/code.zip" "$CODE" 'Kod'
        fi
        echo '----------------------------------------------------------------'
    fi
done < <(echo "$BOOK_TITLES" | sed "s|title|\ntitle|g")
IFS=$SAVEIFS
#wyloguj
printYellow "Wylogowuje ..."
wget --load-cookies "$COOKIE_NAME" "$FQDN/logout"
clean
