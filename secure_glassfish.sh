#!/bin/bash
echo 'YOU WILL NEED - AT LEAST - GLASSFISH 4.1.2!'
ADMIN_USER='admin'
ADMIN_PASSWORD='password'
TMP_FILE=/tmp/tmpfile
PASS_TMP_FILE=/tmp/pwdfile
GLS_BIN_ASADMIN="/home/h3x0r/Stuff/as/glassfish/4.1.2/glassfish4/bin"
# DONT USE -E IN ECHO
echo "AS_ADMIN_PASSWORD=''
AS_ADMIN_NEWPASSWORD='$ADMIN_PASSWORD'" > "$TMP_FILE"
echo "AS_ADMIN_PASSWORD='$ADMIN_PASSWORD'" > "$PASS_TMP_FILE"

#asadmin start-domain
echo 'Change'
"${GLS_BIN_ASADMIN}/asadmin" --user $ADMIN_USER --passwordfile="$TMP_FILE" change-admin-password
echo 'Enable'
"${GLS_BIN_ASADMIN}/asadmin" --user $ADMIN_USER --passwordfile="$PASS_TMP_FILE" enable-secure-admin
#"${GLS_BIN_ASADMIN}/asadmin" --user $ADMIN_USER  enable-secure-admin
#asadmin restart-dom

rm  "$TMP_FILE" "$PASS_TMP_FILE"
