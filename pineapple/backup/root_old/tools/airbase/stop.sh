#!/bin/bash
pkill airbase-ng
/etc/init.d/dnsmasq stop
airmon-ng stop wlan0mon
/etc/init.d/ifprepare stop
echo 'Stopped' >> airbase.log
