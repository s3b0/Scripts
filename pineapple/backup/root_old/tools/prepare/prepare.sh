#!/bin/busybox sh
DEAMONS="atd dnsmasq php5-fpm pineapple telnet cron nginx pineap autossh"
#DEAMONS="telnet cron autossh"
invoke_init()
{
	/etc/init.d/$2 $1 2> /dev/null
}
disable_deamon()
{
	echo "Disabling: $1"
	invoke_init 'stop' "$1"
	invoke_init 'disable' "$1"
}
for deamon in ${DEAMONS}
do
	disable_deamon $deamon			
done
# --------------------------------
move_config_file()
{
	echo "Coping file: $1 to /etc/config"
	cp -f "$1" "/etc/config/$1"
}
FILES="system wireless network"
#FILES="system"
for file in ${FILES}
do
        move_config_file $file
done

invoke_init restart network

cp -f authorized_keys /root/.ssh/authorized_keys

#opkg update
#opkg install vim ip

reboot
